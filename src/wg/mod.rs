pub(crate) mod wghelper;

use std::{fs::File, io::Write, process::Command};
use wghelper::*;

const TEMP: &str = "/tmp/speed.tmp";

pub struct Service<'a> {
    interface: &'a str,
    config: Conf,
}

impl<'a> Service<'a> {
    pub fn new(interface: &'a str) -> Self {
        let config_file = format!("./{}.toml", interface);
        let config = wghelper::toml_to_config(&config_file);
        Self { interface, config }
    }

    pub fn start(&self) {
        Command::new("wg-quick")
            .args(["up", self.interface])
            .status()
            .expect("Failed to run wg-quick");
    }

    pub fn stop(&self) {
        Command::new("wg-quick")
            .args(["down", self.interface])
            .status()
            .expect("Failed to stop wg-quick");
    }

    pub fn add_peer(&mut self, address: &str) {
        self.config.add_peer(address);
    }

    pub fn refresh(&self) {
        // Run wg syncconf wg0 <(wg-quick strip wg0)
        let output = Command::new("wg-quick")
            .args(["strip", "wg0"])
            .output()
            .unwrap();

        let mut tmp = File::create(TEMP).unwrap();
        tmp.write_all(&output.stdout).unwrap();

        Command::new("wg")
            .args(["syncconf", "wg0", TEMP])
            .status()
            .unwrap();
    }

    pub fn config(&self) {
        println!("{:#?}", self.config);
    }

    pub fn remove_peer(&mut self, address: &str) {
        self.config.remove_peer(address);
    }

    pub fn save(&self) {
        wghelper::config_to_toml(&self.config);
    }

    pub fn generate_wg_config(&self) {
        wghelper::genrate_server_config(&self.config);
    }

    pub fn genenrate_peer_config(&self, id: usize) {
        wghelper::generate_peer_config(id, &self.config);
    }
}
