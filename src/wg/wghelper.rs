use std::{
    fs::{self, File},
    io::Write as _,
    fmt::Write as _,
    path::Path,
    process::{Command, Stdio},
};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
struct ServerConf {
    address: String,
    port: u16,
    private_key: String,
    public_key: String,
    post_up: String,
    post_down: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct PeerConf {
    address: String,
    private_key: String,
    public_key: String,
    allowed_ips: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Conf {
    server: ServerConf,
    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    peers: Vec<PeerConf>,
}

impl Conf {
    pub fn add_peer(&mut self, address: &str) {
        let index = self
            .peers
            .iter()
            .position(|peer_conf| peer_conf.address == address);

        if index.is_some() {
            eprintln!("Duplicate peer {} found", address);
            return;
        }

        let (private_key, public_key) = get_keys();
        let new_peer = PeerConf {
            address: address.into(),
            private_key,
            public_key,
            allowed_ips: vec!["10.10.10.0/24".into()],
        };

        self.peers.push(new_peer)
    }

    pub fn remove_peer(&mut self, address: &str) {
        let index = self
            .peers
            .iter()
            .position(|peer_conf| peer_conf.address == address);

        if let Some(index) = index {
            self.peers.remove(index);
        } else {
            eprintln!("No peer {} found", address);
        }
    }
}

fn get_keys() -> (String, String) {
    let output = Command::new("wg")
        .arg("genkey")
        .output()
        .expect("Failed to execute wg genkey");

    let private_key =
        String::from_utf8(output.stdout).unwrap().trim().to_string();

    let mut command = Command::new("wg")
        .arg("pubkey")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to execute wg pubkey");

    command
        .stdin
        .as_mut()
        .expect("Failed to get stdin for wg pubkey")
        .write_all(private_key.as_bytes())
        .expect("Failed to write private key to wg pubkey");

    let output = command
        .wait_with_output()
        .expect("Failed to get output from wg pubkey");

    let public_key =
        String::from_utf8(output.stdout).unwrap().trim().to_string();

    (private_key, public_key)
}

pub fn config_to_toml(conf: &Conf) {
    let toml = toml::to_string(&conf).unwrap();
    let mut wg_config = File::create("wg0.toml").unwrap();
    wg_config.write_all(toml.as_bytes()).unwrap();
}

pub fn toml_to_config<P: AsRef<Path>>(path: P) -> Conf {
    let conf = fs::read_to_string(path).unwrap();
    let conf: Conf = toml::from_str(&conf).unwrap();
    conf
}

pub fn genrate_server_config(conf: &Conf) {
    let mut wg_config = File::create("/etc/wireguard/wg0.conf").unwrap();
    let mut output = String::new();

    output.push_str("[Interface]\n");
    writeln!(&mut output, "Address = {}", conf.server.address).unwrap();
    writeln!(&mut output, "ListenPort = {}", conf.server.port).unwrap();
    writeln!(&mut output, "PrivateKey = {}", conf.server.private_key).unwrap();
    writeln!(&mut output, "PostUp = {}", conf.server.post_up).unwrap();
    writeln!(&mut output, "PostDown = {}", conf.server.post_down).unwrap();

    for peer in &conf.peers {
        output.push_str("\n[Peer]\n");
        writeln!(&mut output, "PublicKey = {}", peer.public_key).unwrap();
        writeln!(&mut output, "AllowedIPs = {}", peer.address).unwrap();
    }

    wg_config.write_all(output.as_bytes()).unwrap();
}

pub fn generate_peer_config(id: usize, conf: &Conf) {
    let mut output = String::new();

    let peer = conf.peers.get(id).unwrap();
    writeln!(&mut output, "[Interface]").unwrap();
    writeln!(&mut output, "Address = {}", peer.address).unwrap();
    writeln!(&mut output, "PrivateKey = {}", peer.private_key).unwrap();

    output.push_str("\n[Peer]\n");
    writeln!(&mut output, "PublicKey = {}", conf.server.public_key).unwrap();
    writeln!(&mut output, "AllowedIPs = {}", peer.allowed_ips[0]).unwrap();
    writeln!(
        &mut output,
        "Endpoint = 140.238.242.140:{}",
        conf.server.port
    )
    .unwrap();
    output.push_str("PersistentKeepalive = 10");

    println!("{}", output);
}
